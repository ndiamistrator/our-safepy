from setuptools import setup

from setuptools.command.install_scripts import install_scripts
import sys

def log(*args):
    print(':::', *args, file=sys.stderr)

class Install(install_scripts):
    def __init__(S, dist):
        super().__init__(dist)
        log('instance:', S)
        log('distribution:', dist)
        log('instance.__dict__:', S.__dict__)
        for k,v in dist.__dict__.items():
            log(f'distribution.__dict__[{k!r}] = {v!r}')

if __name__ == '__main__':
    setup()


#from setuptools import setup
#if __name__ == '__main__':
#    setup(setup_requires=['pbr'], pbr=True)


# # generic setup.py (without pbr / setup.cfg)
# 
# from setuptools import setup
# #from setuptools import find_namespace_packages
# #from distutils import sysconfig


# if __name__ == '__main__':
#     setup(
#         name              = 'our.safepy',
#         url               = 'git+https://gitlab.com/ndiamistrator/our-safepy.git',
#         description       = 'simulate -P command line flag for python < v3.11',
# 
#         use_scm_version   = True,
# 
#         setup_requires    = [
#                                 'setuptools_scm',
#                             ],
# 
#         #packages          = [
#         #                        *find_namespace_packages(where='.',include=['our']),
#         #                    ],
# 
#         #data_files        = [
#         #                        (sysconfig.get_python_lib(), [
#         #                        ]),
#         #                    ],
# 
#         #install_requires  = [
#         #                    ],
# 
#         entry_points      = {
#                                 'console_scripts': [
#                                     'safepy = our.safepy:safepy',
#                                 ],
#                             },
# 
#         #options           = {
#         #                        'bdist_wheel': {
#         #                            'universal': '1',
#         #                        },
#         #                    },
# 
#         cmdclass          = {
#                                 'install_scripts': Install,
#                             },
#     )
