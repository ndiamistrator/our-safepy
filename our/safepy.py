#!/usr/bin/python3 -I

from ._safepy_version import __version__, __version_tuple__

import our

import runpy
import sys
import os


__all__ = 'safepy'


def _log(*args):
    print('safepy:', *args, file=sys.stderr)


def safepy():
    _log(f'sys.argv: {sys.argv}')
    _log(f'sys.path: {sys.path}')
    _log(f'sys.path: {sys.flags}')
    if sys.argv[0] == __file__: # called as -m our.safepy => sys.path[0] == cwd
        cwd = os.getcwd()
        if sys.path[0] == cwd:
            #_log('called as module')
            del sys.path[0]
        else:
            _log(f'seem to be called as module, but path[0]:{sys.path[0]!r} != getcwd():{cwd!r}')
    else: # called as [.../]safepy => sys.path[0] == '.../'
        script_dirname = os.path.dirname(sys.argv[0])
        if sys.path[0] == script_dirname:
            #_log('called as script')
            del sys.path[0]
        else:
            _log(f'seem to be called as script, but path[0]:{sys.path[0]!r} != dirname(argv[0]):{script_dirname!r}')

    if len(sys.argv) < 2:
        code.interact()

    try:
        argv1 = sys.argv[1]
    except IndexError:
        #_log('noop')
        sys.exit()

    if argv1.startswith('-m'):
        if sys.argv[1] != '-m':
            module_name = sys.argv[1][2:]
            del sys.argv[:1]
        else:
            module_name = sys.argv[2]
            del sys.argv[:2]

        #_log('module:', module_name)
        runpy.run_module(module_name, alter_sys=True)

    else:

        script_path = os.path.abspath(sys.argv[1])
        del sys.argv[0]

        #_log('script:', script_path)
        runpy.run_path(script_path, run_name='__main__')


if __name__ == '__main__':
    safepy()
